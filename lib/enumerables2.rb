require 'byebug'

# EASY

# Define a method that returns the sum of all the elements in its argument (an
# array of numbers).
def array_sum(arr)
  arr.reduce(0) { |acc, num| acc + num }
end

# Define a method that returns a boolean indicating whether substring is a
# substring of each string in the long_strings array.
# Hint: you may want a sub_tring? helper method
def in_all_strings?(long_strings, substring)
  if string_counter(long_strings, substring) == long_strings.length
    true
  else
    false
  end
end

def string_counter(long_strings, substring)
  long_strings.reduce(0) do |count, string|
    if string.include?(substring)
      count + 1
    else
      count
    end
  end
end

# Define a method that accepts a string of lower case words (no punctuation) and
# returns an array of letters that occur more than once, sorted alphabetically.
def non_unique_letters(string)
  letter_count = Hash.new(0)
  string.split("").each do |letter|
    next if letter == " "
    letter_count[letter] += 1
  end
  reoccuring_letters = letter_count.select { |_, v| v > 1 }
  reoccuring_letters.keys.sort
end

# Define a method that returns an array of the longest two words (in order) in
# the method's argument. Ignore punctuation!
def longest_two_words(string)
  sorted_arr = string.split.sort_by { |word| word.length }
  sorted_arr.reverse[0..1]
end

# MEDIUM

# Define a method that takes a string of lower-case letters and returns an array
# of all the letters that do not occur in the method's argument.
def missing_letters(string)
  alphabet_arr = ("a".."z").to_a.join
  missing_arr = alphabet_arr.delete(string)
  missing_arr.split("")
end

# Define a method that accepts two years and returns an array of the years
# within that range (inclusive) that have no repeated digits. Hint: helper
# method?
def no_repeat_years(first_yr, last_yr)
  [].tap do |year_arr|
    (first_yr..last_yr).to_a.each do |year|
      if not_repeat_year?(year)
        year_arr << year
      end
    end
  end
end

def not_repeat_year?(year)
  year.to_s.split("").uniq == year.to_s.split("")
end
no_repeat_years(1970, 1980)

# HARD

# Define a method that, given an array of songs at the top of the charts,
# returns the songs that only stayed on the chart for a week at a time. Each
# element corresponds to a song at the top of the charts for one particular
# week. Songs CAN reappear on the chart, but if they don't appear in consecutive
# weeks, they're "one-week wonders." Suggested strategy: find the songs that
# appear multiple times in a row and remove them. You may wish to write a helper
# method no_repeats?
def one_week_wonders(songs)
  songs.each_with_index do |song, idx|
    if songs[idx] == songs[idx + 1]
      songs.delete(song)
    end
  end
  songs.uniq
end

# Define a method that, given a string of words, returns the word that has the
# letter "c" closest to the end of it. If there's a tie, return the earlier
# word. Ignore punctuation. If there's no "c", return an empty string. You may
# wish to write the helper methods c_distance and remove_punctuation.

def for_cs_sake(string)
  string.gsub!(/[!&:;"',.?]/, "")
  return "" unless string.include?("c")
  c_array = c_words(string)

  c_count = Hash.new(0)
  c_array.each do |word|
    c_count[word] = c_distance(word)
  end
  c_count.sort_by { |_, v| v }[0][0]
end

def c_words(string)
  [].tap do |c_word|
    string.split.each do |word|
      if word.include?("c")
        c_word << word
      end
    end
  end
end

def c_distance(word)
  word.reverse.split("").each_with_index do |letter, idx|
    if letter == "c"
      return idx
    end
  end
end

# Define a method that, given an array of numbers, returns a nested array of
# two-element arrays that each contain the start and end indices of whenever a
# number appears multiple times in a row. repeated_number_ranges([1, 1, 2]) =>
# [[0, 1]] repeated_number_ranges([1, 2, 3, 3, 4, 4, 4]) => [[2, 3], [4, 6]]

def repeated_number_ranges(arr)
  counter = 1
  idx = 0
  [].tap do |nested_arr|
    while idx < arr.length
      if arr[idx] == arr[idx + counter]
        until arr[idx] != arr[idx + counter]
          counter += 1
        end
        nested_arr << [idx, idx + counter - 1]
      end
      idx = idx + counter
      counter = 1
    end
  end
end
